#!/bin/bash
#! All lines starting with '#!' are comments, ones with '#' are directives
#! Specify name for the job
#SBATCH -J test_multithread_job

#! Which accout to use (users can be part of one or more accounts). Currently we have only two: 'bioinf' and 'it-support'.
#SBATCH -A bioinf

#! How many CPU to allocate
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8

#! How much time does job need (global default is currently set to 14 days)?
#SBATCH --time=330:00:00

#! Set total memory requested
#SBATCH --mem=10G           # total memory per node

#! Email notification for job conditions i.e. START, END,FAIL, HOLD or none
#SBATCH --mail-type=END,FAIL

#! Do not re-queue job after system fail
#SBATCH --no-requeue

#! Partition to run on
#SBATCH -p production

#! job array parameter
#SBATCH -a 1-2
#!SBATCH -a 1-160 

#! Specify output file
#SBATCH -o logs/parallel_arrays_%a.log

#! Specify file to log any errors
#SBATCH -e logs/parallel_arrays_%a.error


# packages to install
# conda create -name bwa
# conda activate ame
# conda install -c "bioconda/label/cf201901" samtools

# submit from 
# ~/linaria_f2_spur_length/scripts$
# with
# sbatch linaria_f2_spur_length/2_alignment/bwa_2.sh

PROJECT_DIR=/projects/evolution_development/Linaria_F2_spur_length/
DATA_DIR=${PROJECT_DIR}/raw_data/LINrsqzR/soapnuke/clean/
WORK_DIR=${PROJECT_DIR}/results/bwatemp/
TEMP_DIR=/production/bjf36

# source ~/miniconda3/etc/profile.d/conda.sh
# conda activate ame

reference_fasta=/projects/evolution_development/Linaria_F2_spur_length/lv_ref/ncbi_dataset/data/GCA_948329865.1/GCA_948329865.1_daLinVulg1.1_genomic.fna
# bwa index ${reference_fasta}


sample_list_file=${WORK_DIR}/sample_list.txt

array_id=$SLURM_ARRAY_TASK_ID

echo "This is task number $array_id"

echo "Running on:"
hostname

sample_id=`cat ${sample_list_file} | head -n ${array_id} | tail -1`

echo "sample ID is ${sample_id}"

read1_fq=`ls ${DATA_DIR}/${sample_id}/*1.fq.gz`
read2_fq=`ls ${DATA_DIR}/${sample_id}/*2.fq.gz`

output_bam=${TEMP_DIR}/${sample_id}_sorted.bam

N_CPU=8

~/bin/bwa/bwa mem -t ${N_CPU} ${reference_fasta} ${read1_fq} ${read2_fq} | samtools view -b -F 4 - \
 | samtools sort -T ${TEMP_DIR}/tmp_${sample_id} -o ${output_bam}

samtools flagstat ${output_bam} >${output_bam}.flagstat

