# use conda to install GATK
https://gatk.broadinstitute.org/hc/en-us/articles/360035889851--How-to-Install-and-use-Conda-for-GATK4

~/bin/gatk-4.2.6.1$ conda env create -n gatk -f gatkcondaenv.yml

# other software installation
https://gatk.broadinstitute.org/hc/en-us/articles/360041320571--How-to-Install-all-software-packages-required-to-follow-the-GATK-Best-Practices

# Data pre-processing for variant discovery
https://gatk.broadinstitute.org/hc/en-us/articles/360035535912-Data-pre-processing-for-variant-discovery

# Best Practices Workflows for Germline short variant discovery (SNPs + Indels)
https://gatk.broadinstitute.org/hc/en-us/articles/360035535932-Germline-short-variant-discovery-SNPs-Indels-

# clean reads and mapping
https://gatk.broadinstitute.org/hc/en-us/articles/360039568932--How-to-Map-and-clean-up-short-read-sequence-data-efficiently

Step 1:
Add read group to bam files, mark duplicates and call variants per sample/bam file
script: 3_variant_calling/gatk_variant_call_per_sample.sh

Step 2:
Merge the variant calls from multiple samples and further variant calling and filtering
script: 3_variant_calling/gatk_merge_variant_call_from_samples.sh

# output example line
OX415249.1	66539	.	CCT	C	34.23	DP60	AC=2;AF=0.111;AN=18;DP=24;ExcessHet=0.0000;FS=0.000;InbreedingCoeff=0.2557;MLEAC=1;MLEAF=0.056;MQ=48.00;QD=34.23;SOR=1.609	GT:AD:DP:GQ:PGT:PID:PL:PS	./.:5,0:5:0:.:.:0,0,0	1|1:0,1:1:3:1|1:66537_C_A:45,3,0:66537	0/0:2,0:2:6:.:.:0,6,53	0/0:5,0:5:15:.:.:0,15,157	0/0:3,0:3:0:.:.:0,0,41	0/0:3,0:3:9:.:.:0,9,93	0/0:1,0:1:3:.:.:0,3,41	0/0:2,0:2:6:.:.:0,6,63	0/0:1,0:1:3:.:.:0,3,39	0/0:1,0:1:3:.:.:0,3,13

# AC/AF/AN
ChromosomeCounts 	Counts and frequency of alleles in called genotypes (AC, AF, AN)
https://gatk.broadinstitute.org/hc/en-us/articles/360037436491-ChromosomeCounts

# ExcessHet
Phred-scaled p-value for exact test of excess heterozygosity.
https://gatk.broadinstitute.org/hc/en-us/articles/5358873269019-ExcessHet

# FS
FisherStrand 	Strand bias estimated using Fisher's exact test (FS)
https://gatk.broadinstitute.org/hc/en-us/articles/5257873894939-FisherStrand

# InbreedingCoeff
Likelihood-based test for the consanguinity among samples (InbreedingCoeff)
https://gatk.broadinstitute.org/hc/en-us/articles/360036863691-InbreedingCoeff

# MQ
RMSMappingQuality 	Root mean square of the mapping quality of reads across all samples (MQ)
https://gatk.broadinstitute.org/hc/en-us/articles/360057438331-RMSMappingQuality

# QD
QualByDepth 	Variant confidence normalized by unfiltered depth of variant samples (QD)
This annotation puts the variant confidence QUAL score into perspective by normalizing for the amount of coverage available. 
https://gatk.broadinstitute.org/hc/en-us/articles/5257827326747-QualByDepth

# SOR
StrandOddsRatio 	Strand bias estimated by the symmetric odds ratio test (SOR)
https://gatk.broadinstitute.org/hc/en-us/articles/4413056177947-StrandOddsRatio