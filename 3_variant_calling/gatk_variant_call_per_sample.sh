#!/bin/bash
#! All lines starting with '#!' are comments, ones with '#' are directives
#! Specify name for the job
#SBATCH -J variant_call_job_array

#! Which accout to use (users can be part of one or more accounts). Currently we have only two: 'bioinf' and 'it-support'.
#SBATCH -A bioinf

#! How many CPU to allocate
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1

#! How much time does job need (global default is currently set to 14 days)?
#SBATCH --time=100:00:00

#! Set total memory requested
#SBATCH --mem=10G           # total memory per node

#! Email notification for job conditions i.e. START, END,FAIL, HOLD or none
#SBATCH --mail-type=END,FAIL

#! Do not re-queue job after system fail
#SBATCH --no-requeue

#! Partition to run on
#SBATCH -p production

#! job array parameter
#SBATCH -a 1-10
#!SBATCH -a 3-10
#!SBATCH -a 11-14
#!SBATCH -a 1-160

#! Specify output file
#SBATCH -o logs/parallel_arrays_%a.log

#! Specify file to log any errors
#SBATCH -e logs/parallel_arrays_%a.error

PROJECT_DIR=~/linaria_f2_spur_length/temp

# sample_list_file is the samples to run the job array on
sample_list_file=${PROJECT_DIR}/sample_list.txt
array_id=$SLURM_ARRAY_TASK_ID

# sample_id is the sample for this job
sample_id=`cat ${sample_list_file} | head -n ${array_id} | tail -1`

# This information goes to the log file: logs/parallel_arrays_%a.log
echo "This is task number $array_id running ${sample_id}"
echo "Running on:"
hostname

####################################################
# STEP 0: Set up                                   #
####################################################

# install GATK
# cd ~/bin
# wget https://github.com/broadinstitute/gatk/releases/download/4.2.6.1/gatk-4.2.6.1.zip
# unzip gatk-4.2.6.1.zip

# # -Xmx8g set the max memory used to 8Gb
GATK_BIN='~/bin/gatk-4.2.6.1/gatk --java-options "-Xmx8g"' 
PICARD_BIN="picard -Xms8g -Xmx8g" 

BAM_DIR=${PROJECT_DIR}/sorted_bams
OUTPUT_DIR=${PROJECT_DIR}

TEMP_DIR=/local_data/qw254
# this is where the intermediate file goes
# intermediate files don't need to be kept on the cluster storage system

vcf_dir=${OUTPUT_DIR}/vcf
markdup_stat_dir=${OUTPUT_DIR}/markdup_stats

RG_BAM_DIR=${TEMP_DIR}/RG_bam/

if [ ! -d ${RG_BAM_DIR} ]; then
    mkdir -p ${RG_BAM_DIR}
fi

#reference_fasta=/projects/evolution_development/Linaria_F2_spur_length/lv_ref/ncbi_dataset/data/GCA_948329865.1/GCA_948329865.1_daLinVulg1.1_genomic.fna
reference_fasta=${PROJECT_DIR}/reference/ncbi_dataset/data/GCA_948329865.1/GCA_948329865.1_daLinVulg1.1_genomic.fna
# This reference need to be prepared to be used by GATK with the following two steps:
# Step 1:
# ${GATK_BIN} CreateSequenceDictionary -R ${reference_fasta}
# Step 2:
# conda activate ame
# samtools faidx ${reference_fasta}
# More information here:
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531652-FASTA-Reference-genome-format

bam_file=${BAM_DIR}/${sample_id}_sorted.bam
bam_RG_file=${RG_BAM_DIR}/${sample_id}_sorted_RG.bam
# when running on cluster it will be more efficient to have this one stored in /local_data

bam_markdup_file=${OUTPUT_DIR}/markdup_bam/${sample_id}_sorted_markdup.bam
# the file with duplicates marked

chr_id=OX415249.1 # this script is for testing, thus variant call only to chromosome 1

####################################################
# STEP 1: Add read group information to bam files  #
####################################################

# GATK only works with RG (read group) information
# https://gatk.broadinstitute.org/hc/en-us/articles/360035890671-Read-groups
# we need to add this to the bamfiles
# @RG ID:Lb1  PL:BGI PU:XX    LB:Lb1    PI:0 

# add RG information with Picard while doing mark duplicates
# conda install picard
source ~/miniconda3/etc/profile.d/conda.sh

conda activate picard

picard_RG_command="${PICARD_BIN} AddOrReplaceReadGroups \
     I=${bam_file} \
     O=${bam_RG_file} \
     RGID=${sample_id} \
     RGLB=${sample_id} \
     RGPL=BGI \
     RGPU=XX \
     RGSM=${sample_id}"


eval ${picard_RG_command} 

# conda activate ame
# samtools index ${bam_RG_file}

####################################################
# STEP 2: Mark duplicates                          #
####################################################

# default: --REMOVE_DUPLICATES=false, 
# so the original bam can be removed from the stroage system

conda activate picard
picard_markdup_command="${PICARD_BIN} MarkDuplicates \
     I=${bam_RG_file} \
     O=${bam_markdup_file} \
     M=${markdup_stat_dir}/${sample_id}_duplicates_metrics.txt \
     ASSUME_SORTED=true"

# only run if it the output does not exist
if [ ! -e ${bam_markdup_file} ]
    then 
    eval ${picard_markdup_command}
fi

####################################################
# STEP 3: Call variants(GVCF) from the bam file    #
####################################################
# about GVCF:
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531812-GVCF-Genomic-Variant-Call-Format

conda activate ame # in order to use samtools
samtools index ${bam_markdup_file}

conda deactivate # in order to use GATK

variant_call_file=${vcf_dir}/${sample_id}_${chr_id}.vcf.gz

gatk_vc_command="${GATK_BIN} HaplotypeCaller \
    -I ${bam_markdup_file} \
    -R ${reference_fasta} \
    -ERC GVCF \
    -L ${chr_id} \
    -O ${variant_call_file}"

# only run if it the output does not exist
if [ ! -e ${variant_call_file} ]
    then 
    eval ${gatk_vc_command}
fi